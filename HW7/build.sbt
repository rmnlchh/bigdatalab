name := "hw1kafka"

version := "0.1"

scalaVersion := "2.13.4"

libraryDependencies += "org.apache.kafka" %% "kafka" % "2.7.0"
libraryDependencies += "org.apache.zookeeper" % "zookeeper" % "3.6.2"

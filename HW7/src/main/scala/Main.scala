
object Main {
  def main(args: Array[String]): Unit = {
    val brokers = args(0)
    val topic = args(1)
    val speakers = args(2).split(",")
    val path = args(3)
    val producer = new ProducerTask(brokers, topic, speakers, path)
    producer.start()
  }
}

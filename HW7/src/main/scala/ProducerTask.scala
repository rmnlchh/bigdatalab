
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord, RecordMetadata}

import java.time.LocalDateTime
import java.util.Properties
import scala.collection.mutable
import scala.io.Source
import scala.util.Random

class Producer(br: String, t: String, sp: Array[String], path: String) {
  var topic: String = t
  var brokers: String = br
  var speakers: Array[String] = sp
  var filePath: String = path
  var messagesPerUser: mutable.Map[String, LocalDateTime] = scala.collection.mutable.Map[String, LocalDateTime]()

  val kafkaProperties = new Properties()
  kafkaProperties.put("bootstrap.servers", brokers)
  kafkaProperties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
  kafkaProperties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")

  val producer = new KafkaProducer[String, String](kafkaProperties)

  def start(): Unit = {
    val cl: ClassLoader = this.getClass.getClassLoader

    val dictionary: List[String] = Source.fromFile(cl.getResource(filePath).getFile).getLines().toList.take(27)
    var time: LocalDateTime = LocalDateTime.now()

    while (true) {
      try {
        time = time.plusSeconds(Random.nextInt(20))

        val msg = new Message(speakers(Random.nextInt(speakers.length)),
          dictionary(Random.nextInt(dictionary.size)), java.sql.Timestamp.valueOf(time).getTime)
        messagesPerUser += msg.getSpeaker -> time

        val objectMapper = new ObjectMapper();
        objectMapper.registerModule(DefaultScalaModule)
        val finalMsg = objectMapper.writeValueAsString(msg)

        val record = new ProducerRecord[String, String](topic, finalMsg)
        producer.send(record, (recordMetaDta: RecordMetadata, e: Exception) => {
          println(recordMetaDta.offset())
        })

        Thread.sleep(1000)
      } catch {
        case e: Exception => e.printStackTrace()
      }
    }

  }
}
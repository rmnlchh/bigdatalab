import com.google.gson.Gson
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.sql.{Column, DataFrame, Encoder, Encoders}
import org.apache.spark.sql.functions.{col, collect_list, count, lit, when, window}
import java.sql.Timestamp

class Consumer(val dataFrame: DataFrame, val windowDuration: String, val banned: Broadcast[Seq[String]]) extends Serializable {
  val censorReplacer: String = "lol"

  val msgEncoder: Encoder[Message] = Encoders.bean(classOf[Message])
  val msgContentEncoder: Encoder[(String, String, Timestamp)] = Encoders.product[(String, String, Timestamp)]

  def getMessages: DataFrame = {
    dataFrame
      .selectExpr("CAST(value AS STRING)")
      .map(x => new Gson().fromJson(x.get(0).asInstanceOf[String], classOf[Message]))(msgEncoder)
      .map(message => (message.getSpeaker, censorWord(message.getWord, banned), new Timestamp(message.getTime)))(msgContentEncoder)
      .toDF("speaker", "word", "time")
  }

  def censorWord(word: String, bannedWords: Broadcast[Seq[String]]): String = {
    if (bannedWords.value.contains(word)) censorReplacer else word
  }

  def windowAndAllWordsPerSpeaker(dataFrame: DataFrame): DataFrame = {
    val durationWindow: Column = window(timeColumn = new Column("time"), windowDuration = this.windowDuration)
    dataFrame
      .withWatermark("time", "120 seconds")
      .groupBy(durationWindow, new Column("speaker"))
      .agg(collect_list("word") as "words")
  }

  def getBannedWordsPerSpeaker(dataFrame: DataFrame): DataFrame = {
    dataFrame
      .groupBy(col("speaker"))
      .agg(count(when(col("word") === lit(censorReplacer), true)) as "censored_count")
  }
}

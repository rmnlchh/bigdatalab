import org.apache.spark.broadcast.Broadcast
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.streaming.Trigger

object Main {
  def main(args: Array[String]): Unit = {

    val bannedWords = args(0)
    val topic = args(1)
    val brokers = args(2)
    val windowDuration = args(3).toInt + " minutes"

    val spark = SparkSession
      .builder
      .appName("KafkaConsumerTask")
      .master("local")
      .getOrCreate()

    spark.sparkContext.setLogLevel("ERROR")

    val dataFrame = spark
      .readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", brokers)
      .option("subscribe", topic)
      .load()

    val source = scala.io.Source.fromFile(bannedWords)
    val bannedWordList: Broadcast[Seq[String]] = spark.sparkContext.broadcast(source.getLines().toList)
    source.close()

    val kafkaConsumer = new Consumer(dataFrame, windowDuration, bannedWordList)

    val messages = kafkaConsumer.getMessages

    kafkaConsumer
      .getBannedWordsPerSpeaker(messages)
      .writeStream
      .outputMode("complete")
      .format("console")
      .start()

    kafkaConsumer
      .windowAndAllWordsPerSpeaker(messages)
      .writeStream
      .outputMode("append")
      .format("console")
      .start()
      .awaitTermination()
  }
}

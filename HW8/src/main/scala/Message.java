import java.io.Serializable;
import java.util.Objects;

public class Message implements Serializable {

    private String speaker;

    private String word;

    private long time;

    public Message() {
    }

    public String getSpeaker() {
        return speaker;
    }

    public void setSpeaker(String speaker) {
        this.speaker = speaker;
    }

    public Message(String speaker, String word, long time) {
        this.speaker = speaker;
        this.word = word;
        this.time = time;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        return Objects.equals(speaker, message.speaker) && Objects.equals(word, message.word) && Objects.equals(time, message.time);
    }

    @Override
    public int hashCode() {
        return Objects.hash(speaker, word, time);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}



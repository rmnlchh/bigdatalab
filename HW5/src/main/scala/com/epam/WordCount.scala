package com.epam

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object WordCount {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf()
      .setAppName("SparkWordCount")
//      .setMaster("local")
    val sc = new SparkContext(conf)
    val textFile = sc.textFile(args(0))
    val res = wordCount(textFile)
    res
      .sortByKey()
      .saveAsTextFile(args(1))

    sc.stop()
  }

  def wordCount(rdd: RDD[String]): RDD[(String, Int)] = {
    rdd
      .flatMap(line => line.split("\\W"))
      .map(_.toLowerCase())
      .filter(!_.isEmpty)
      .map(word => (word, 1))
      .reduceByKey(_ + _)
  }
}

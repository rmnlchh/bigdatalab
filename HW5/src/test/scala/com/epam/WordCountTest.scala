package com.epam

import org.scalatest._
import flatspec._
import matchers._
import org.apache.spark.{SparkConf, SparkContext}

class WordCountTest extends AnyFlatSpec with should.Matchers {

  "Map reduce" should "map and reduce given rdd" in {
    val conf = new SparkConf().setAppName("SparkWordCount").setMaster("local[*]")
    val sc = new SparkContext(conf)

    val rdd = sc.parallelize(Seq[String]("a b a c d d"))
    val map = Map[String, Int]("a" -> 2, "b" -> 1, "c" -> 1, "d" -> 2)
    WordCount.wordCount(rdd).collectAsMap() should be(map)
  }

}

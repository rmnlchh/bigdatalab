
import org.apache.spark.sql.functions.{col, collect_set, lit}
import org.apache.spark.sql.{DataFrame, Dataset, Encoder, SparkSession, functions}

object SparkDataset {


  def mainDataset(awards: Dataset[String], scoring: Dataset[(String, String, String)],
                  teams: Dataset[(String, String)], players: Dataset[(String, String, String)]): DataFrame = {

    val awardsPerPlayer = awards
      .groupBy("playerID")
      .count().alias("awardsPerPlayer")

    val stintsPerPlayer = scoring
      .groupBy("playerID")
      .agg(functions.sum(col("stint")) as "stint").
      alias("stintsPerPlayer")

    val topScorers = stintsPerPlayer
      .orderBy(col("stint").desc)
      .limit(10)

    topScorers
      .join(scoring, "playerID")
      .join(teams, "tmID")
      .select("playerID", "stintsPerPlayer.stint", "tmID", "name")
      .groupBy("playerID", "stint").agg(collect_set("name") as "Teams")
      .join(awardsPerPlayer, Seq("playerID"), "left")
      .join(players, "playerID")
      .orderBy(col("stint").desc)
      .withColumn("Goals", col("stint").cast("Integer"))
      .select(
        functions.concat(col("firstName"), lit(" "), col("lastName")) as "Name",
        col("Goals"),
        col("count") as "Awards",
        col("Teams"))
      .na.fill(0)
  }
}
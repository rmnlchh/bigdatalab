import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{Row, SparkSession}

object SparkRDD {

  def mainRDD(sparkSession: SparkSession, awards: RDD[Row], scoring: RDD[Row],
              teams: RDD[Row], players: RDD[Row]): RDD[(String, Int, Int, Set[String])] = {

    val awardsPerPlayer = awards.map(x => (x.getAs[String]("playerID"), 1)).reduceByKey(_ + _)
    val goalsPerPlayer = scoring.map(x => (x.getAs[String]("playerID"), x.getAs[String]("stint").toInt)).reduceByKey(_ + _)

    val top10Scorers = sparkSession.sparkContext.parallelize(goalsPerPlayer.sortBy(_._2, ascending = false).take(10))

    val playerStats = top10Scorers
      .leftOuterJoin(awardsPerPlayer)
      .map(x => x._1 -> (x._2._1, x._2._2.getOrElse(0)))

    val playerIDAndName = players.map(x => x.getAs[String]("playerID") -> (x.getAs[String]("firstName")
      + " " + x.getAs[String]("lastName")))

    val teamsByID = teams.map(x => x.getAs[String]("tmID") -> x.getAs[String]("name"))

    val playerTeam = scoring
      .map(x => x.getAs[String]("tmID") -> x.getAs[String]("playerID"))
      .join(teamsByID)
      .map(x => x._2._1 -> x._2._2)
      .groupByKey()
      .map(x => x._1 -> x._2.toSet)

    playerStats
      .join(playerTeam)
      .map(x => x._1 -> (x._2._1._1, x._2._1._2, x._2._2))
      .join(playerIDAndName)
      .map(x => (x._2._2, x._2._1._1, x._2._1._2, x._2._1._3))

  }
}

import org.apache.spark.sql.{Dataset, Encoder, SparkSession}

object Main {
  def main(args: Array[String]): Unit = {

    val spark: SparkSession = SparkSession.builder()
      .appName("SparkTask2")
      .master("local")
      .getOrCreate()

    val awardsPlayersRDD = spark.read.option("header", "true").format("csv").load("AwardsPlayers.csv").rdd
    val scoringRDD = spark.read.option("header", "true").format("csv").load("Scoring.csv").rdd
    val teamsRDD = spark.read.option("header", "true").format("csv").load("Teams.csv").rdd
    val playersRDD = spark.read.option("header", "true").format("csv").load("Master.csv").rdd

    val stringEnc: Encoder[String] = org.apache.spark.sql.Encoders.STRING
    val stringStringEnc: Encoder[(String, String)] = org.apache.spark.sql.Encoders.product[(String, String)]
    val stringStringStringEnc: Encoder[(String, String, String)] = org.apache.spark.sql.Encoders.product[(String, String, String)]

    val awards: Dataset[String] = spark.read.format("csv").option("header", "true")
      .load("AwardsPlayers.csv").select("playerID").as[String](stringEnc)
    val scoring: Dataset[(String, String, String)] = spark.read.format("csv").option("header", "true")
      .load("Scoring.csv").select("playerID", "stint", "tmID").as[(String, String, String)](stringStringStringEnc)
    val teams: Dataset[(String, String)] = spark.read.format("csv").option("header", "true")
      .load("Teams.csv").select("tmID", "name").as[(String, String)](stringStringEnc)
    val players: Dataset[(String, String, String)] = spark.read.format("csv").option("header", "true")
      .load("Master.csv").select("playerID", "firstName", "lastName").as[(String, String, String)](stringStringStringEnc)

    val resultRDD = SparkRDD.mainRDD(spark, awardsPlayersRDD, scoringRDD, teamsRDD, playersRDD)
    resultRDD.saveAsTextFile("results/RDD/textResult")

    val resultSQL = SparkSQL.mainSQL(spark, awards, scoring, teams, players)
    resultSQL.write.json("results/SQL/jsonResult")

    val resultDataset = SparkDataset.mainDataset(awards, scoring, teams, players)
    resultDataset.write.parquet("results/Dataset/parquetResult")
    //    resultDataset.write.csv("results/Dataset/csvResult") doesn`t support idk
  }
}

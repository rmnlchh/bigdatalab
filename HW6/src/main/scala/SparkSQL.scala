import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}

object SparkSQL {

  def mainSQL(spark: SparkSession, awards: Dataset[String], scoring: Dataset[(String, String, String)],
              teams: Dataset[(String, String)], players: Dataset[(String, String, String)]): DataFrame = {

    awards.createOrReplaceTempView("awards")
    teams.createOrReplaceTempView("teams")
    players.createOrReplaceTempView("players")
    scoring.createOrReplaceTempView("scoring")

    spark.sql(
      """SELECT playerID, CAST(stint AS INTEGER) as stint, tmID
        | FROM scoring""".stripMargin).createOrReplaceTempView("scoring")

    val awardsPerPlayer = spark.sql(
      """SELECT playerID, count( * ) as awards
        |FROM awards
        |GROUP BY playerID""".stripMargin)

    val topScorers = spark.sql(
      """SELECT playerID, sum(stint) as stint
        |FROM scoring
        |GROUP BY playerID
        |ORDER BY stint DESC
        |LIMIT 10""".stripMargin)

    val playerTeam = spark.sql(
      """SELECT playerID, collect_set(name) as teams
        |FROM scoring
        |JOIN teams ON scoring.tmID = teams.tmID
        |GROUP BY playerID""".stripMargin)
    playerTeam.show()

    topScorers.createOrReplaceTempView("topScore")
    awardsPerPlayer.createOrReplaceTempView("awards")
    playerTeam.createOrReplaceTempView("teams")

    val result = spark.sql(
      """SELECT *
        |FROM topScore
        |LEFT JOIN awards ON topScore.playerID = awards.playerID
        |JOIN teams ON topScore.playerID = teams.playerID
        |JOIN players ON topScore.playerID = players.playerID""".stripMargin)
    result.createOrReplaceTempView("result")

    spark.sql(
      """SELECT CONCAT(firstName,CONCAT(" ",lastName)) as Name, IFNULL(awards, 0) as Awards, stint as Goals, teams
        |FROM result""".stripMargin)
  }
}
create table idname as
select id, name
from hotels;

create table visits as
select h.id, h.name as name, e.srch_ci, e.srch_co
from expedia e
         inner join
     idname h
     on (e.hotel_id = h.id);

create table visit_count as
select name, month(srch_ci) as month
from visits
union
select name, month(srch_co) as month
from visits;

create table groupedvisits as
select name, month, count(*) as count
from visit_count
group by name, month;

select *
from (
         select name, month, count, rank() over (partition by month, name order by count desc) as rk
         from groupedvisits
         order by count
     ) a
where rk <= 10;

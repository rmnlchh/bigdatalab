create table diff as
select max(avg_tmpr_c) - min(avg_tmpr_c) as difference, month, year, lat, lng,
from weather
group by year, month, lat, lng;

create table hotels1 as
select id, name, latitude, longtitude
from hotels;

create table result as
select h.id as id, h.name as name, d.difference as difference, d.month as month, d.year as year
from hotels1 h
         inner join
     diff d
     on (abs(h.latitude - d.lat) < 0.1 and abs(h.longtitude - d.lng) < 0.1)
group by id, name, month, year, difference
order by difference;

select *
from result limit 10;
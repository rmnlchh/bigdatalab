create table longvisits as
select id, hotel_id as hid, srch_ci, srch_co
from expedia
where datediff(srch_co, srch_ci) > 7;

create table hotelvisits as
select v.id as vid, h.id as hid, h.name as name, h.longtitude as long, h.latitude as lati, v.srch_ci, v.srch_co
from hotels h
         inner join
     longvisits v
     on (h.id = v.hid)
where h.longtitude is not null
  and h.latitude is not null;

create table hvd as
select vid, hid, name, lati, long, srch_ci
from hotelvisits
union
select vid, hid, name, lati, long, srch_co
from hotelvisits;

create table hvdw as
select h.vid as vid, h.name as name, h.srch_ci as d, w.tmp as t
from hvd h
         inner join
     weather3 w
     on (abs(h.lati - w.lat) < 0.1 and abs(h.long - w.lng) < 0.1 and h.srch_ci = w.wthr_date)

select vid, name, max(t) - min(t) as difference, avg(t) as average
from hvdw
group by vid, name;

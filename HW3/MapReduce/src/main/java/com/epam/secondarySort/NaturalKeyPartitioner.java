package com.epam.secondarySort;

import org.apache.hadoop.mapreduce.Partitioner;

public class NaturalKeyPartitioner extends Partitioner<CompositeKey, CompositeValue> {

    @Override
    public int getPartition(CompositeKey compositeKey, CompositeValue compositeValue, int i) {
        return (int) (compositeKey.getHotel_id()% i);
    }
}
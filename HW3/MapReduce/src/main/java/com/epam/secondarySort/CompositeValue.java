package com.epam.secondarySort;

import org.apache.hadoop.io.Writable;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Objects;

public class CompositeValue implements Writable {
    private String lastVisited;
    private int channel;

    public String getLastVisited() {
        return lastVisited;
    }

    public void setLastVisited(String lastVisited) {
        this.lastVisited = lastVisited;
    }

    public int getChannel() {
        return channel;
    }

    public void setChannel(int channel) {
        this.channel = channel;
    }

    public CompositeValue() {
        // :)
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeUTF(lastVisited);
        dataOutput.writeInt(channel);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        lastVisited = dataInput.readUTF();
        channel = dataInput.readInt();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompositeValue that = (CompositeValue) o;
        return channel == that.channel &&
                Objects.equals(lastVisited, that.lastVisited);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lastVisited, channel);
    }

    @Override
    public String toString() {
        return "CompositeValue{" +
                "lastVisit=" + lastVisited +
                ", channel=" + channel +
                '}';
    }
}

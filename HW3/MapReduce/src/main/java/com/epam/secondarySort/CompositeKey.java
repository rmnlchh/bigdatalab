package com.epam.secondarySort;

import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Comparator;
import java.util.Objects;


public class CompositeKey implements WritableComparable<CompositeKey> {

    private long hotel_id;
    private String srch_ci;
    private long booking_id;

    public long getHotel_id() {
        return hotel_id;
    }

    public void setHotel_id(long hotel_id) {
        this.hotel_id = hotel_id;
    }

    public String getSrch_ci() {
        return srch_ci;
    }

    public void setSrch_ci(String srch_ci) {
        this.srch_ci = srch_ci;
    }

    public long getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(long booking_id) {
        this.booking_id = booking_id;
    }

    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeLong(hotel_id);
        dataOutput.writeUTF(srch_ci);
        dataOutput.writeLong(booking_id);
    }

    public void readFields(DataInput dataInput) throws IOException {
        hotel_id = dataInput.readLong();
        srch_ci = dataInput.readUTF();
        booking_id = dataInput.readLong();
    }

    public int compareTo(CompositeKey o) {
        return Comparator.comparing(CompositeKey::getHotel_id)
                .thenComparing(CompositeKey::getSrch_ci)
                .thenComparingLong(CompositeKey::getBooking_id)
                .compare(this, o);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompositeKey that = (CompositeKey) o;
        return hotel_id == that.hotel_id &&
                booking_id == that.booking_id &&
                Objects.equals(srch_ci, that.srch_ci);
    }

    @Override
    public int hashCode() {
        return Objects.hash(hotel_id, srch_ci, booking_id);
    }

    @Override
    public String toString() {
        return "CompositeKey{" +
                "hotel_id=" + hotel_id +
                ", srch_ci=" + srch_ci +
                ", booking_id=" + booking_id +
                '}';
    }
}
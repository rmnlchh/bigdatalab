package com.epam;

import com.epam.secondarySort.*;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.mapred.AvroKey;
import org.apache.avro.mapreduce.AvroJob;
import org.apache.avro.mapreduce.AvroKeyInputFormat;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.StreamSupport;


public class Main {
    private static Schema parseSchema() {
        Schema.Parser parser = new Schema.Parser();
        return parser.parse("{\n" +
                "  \"type\" : \"record\",\n" +
                "  \"name\" : \"topLevelRecord\",\n" +
                "  \"fields\" : [ {\n" +
                "    \"name\" : \"id\",\n" +
                "    \"type\" : [ \"long\", \"null\" ]\n" +
                "  }, {\n" +
                "    \"name\" : \"date_time\",\n" +
                "    \"type\" : [ \"string\", \"null\" ]\n" +
                "  }, {\n" +
                "    \"name\" : \"site_name\",\n" +
                "    \"type\" : [ \"int\", \"null\" ]\n" +
                "  }, {\n" +
                "    \"name\" : \"posa_continent\",\n" +
                "    \"type\" : [ \"int\", \"null\" ]\n" +
                "  }, {\n" +
                "    \"name\" : \"user_location_country\",\n" +
                "    \"type\" : [ \"int\", \"null\" ]\n" +
                "  }, {\n" +
                "    \"name\" : \"user_location_region\",\n" +
                "    \"type\" : [ \"int\", \"null\" ]\n" +
                "  }, {\n" +
                "    \"name\" : \"user_location_city\",\n" +
                "    \"type\" : [ \"int\", \"null\" ]\n" +
                "  }, {\n" +
                "    \"name\" : \"orig_destination_distance\",\n" +
                "    \"type\" : [ \"double\", \"null\" ]\n" +
                "  }, {\n" +
                "    \"name\" : \"user_id\",\n" +
                "    \"type\" : [ \"int\", \"null\" ]\n" +
                "  }, {\n" +
                "    \"name\" : \"is_mobile\",\n" +
                "    \"type\" : [ \"int\", \"null\" ]\n" +
                "  }, {\n" +
                "    \"name\" : \"is_package\",\n" +
                "    \"type\" : [ \"int\", \"null\" ]\n" +
                "  }, {\n" +
                "    \"name\" : \"channel\",\n" +
                "    \"type\" : [ \"int\", \"null\" ]\n" +
                "  }, {\n" +
                "    \"name\" : \"srch_ci\",\n" +
                "    \"type\" : [ \"string\", \"null\" ]\n" +
                "  }, {\n" +
                "    \"name\" : \"srch_co\",\n" +
                "    \"type\" : [ \"string\", \"null\" ]\n" +
                "  }, {\n" +
                "    \"name\" : \"srch_adults_cnt\",\n" +
                "    \"type\" : [ \"int\", \"null\" ]\n" +
                "  }, {\n" +
                "    \"name\" : \"srch_children_cnt\",\n" +
                "    \"type\" : [ \"int\", \"null\" ]\n" +
                "  }, {\n" +
                "    \"name\" : \"srch_rm_cnt\",\n" +
                "    \"type\" : [ \"int\", \"null\" ]\n" +
                "  }, {\n" +
                "    \"name\" : \"srch_destination_id\",\n" +
                "    \"type\" : [ \"int\", \"null\" ]\n" +
                "  }, {\n" +
                "    \"name\" : \"srch_destination_type_id\",\n" +
                "    \"type\" : [ \"int\", \"null\" ]\n" +
                "  }, {\n" +
                "    \"name\" : \"hotel_id\",\n" +
                "    \"type\" : [ \"long\", \"null\" ]\n" +
                "  } ]\n" +
                "}\n");
    }

    public static final Schema SCHEMA = parseSchema();
//    public static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public static class DataMapper extends Mapper<AvroKey<GenericData.Record>, NullWritable, CompositeKey, CompositeValue> {
        @Override
        public void map(AvroKey<GenericData.Record> key, NullWritable value, Context context) throws IOException, InterruptedException {
            CompositeKey jobKey = new CompositeKey();
            CompositeValue jobValue = new CompositeValue();

            if ((int) key.datum().get("srch_adults_cnt") < 2) {
                return;
            }

            jobKey.setHotel_id((long) key.datum().get("hotel_id"));
            String tmpStr = (String) key.datum().get("srch_ci");
            if (tmpStr == null) {
                return;
            }
            jobKey.setSrch_ci(tmpStr);
            jobKey.setBooking_id((long) key.datum().get("id"));

            tmpStr = (String) key.datum().get("srch_co");
            if (tmpStr == null) {
                return;
            }
            jobValue.setLastVisited(tmpStr);
            jobValue.setChannel((int) key.datum().get("channel"));
            context.write(jobKey, jobValue);
        }
    }

    public static class VisitReducer extends Reducer<CompositeKey, CompositeValue, Text, IntWritable> {
        public void reduce(CompositeKey key, Iterable<CompositeValue> values, Context context) throws IOException, InterruptedException {
//            context.write(new Text(String.valueOf(key.getHotel_id())), new IntWritable(StreamSupport.stream(values.spliterator(), false)
//                    .max(Comparator.comparing(x -> LocalDate.parse(x.getLastVisited())))
//                    .get().getChannel()));
            CompositeValue maxValue = new CompositeValue();
            maxValue.setLastVisited(LocalDate.MIN.toString());
            for (CompositeValue value : values) {
                if (value.getLastVisited().compareTo(maxValue.getLastVisited()) > 0) {
                    maxValue.setLastVisited(value.getLastVisited());
                    maxValue.setChannel(value.getChannel());
                }
            }
            context.write(new Text(String.valueOf(key.getHotel_id())), new IntWritable(maxValue.getChannel()));
        }
    }

    public static void main(String[] args) throws Exception {
        if (args.length != 2) {
            System.err.println("Usage: MapReduce <input path> <output path>");
            System.exit(-1);
        }
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "search MapRed");
        job.setJarByClass(Main.class);

        //INPUT
        FileSystem fs = FileSystem.get(conf);
        List<Path> avroFiles = new LinkedList<>();
        RemoteIterator<LocatedFileStatus> fileStatusListIterator = fs.listFiles(new Path(args[0]), true);
        while (fileStatusListIterator.hasNext()) {
            LocatedFileStatus fileStatus = fileStatusListIterator.next();
            if (fileStatus.getPath().toString().endsWith(".avro")) {
                avroFiles.add(fileStatus.getPath());
            }
        }
        Path[] avroFilesArr = new Path[avroFiles.size()];
        int i = 0;
        for (Path file : avroFiles) {
            avroFilesArr[i++] = file;
        }
        FileInputFormat.setInputPaths(job, avroFilesArr);
        job.setInputFormatClass(AvroKeyInputFormat.class);
        AvroJob.setInputKeySchema(job, Main.SCHEMA);

        //Mapper
        job.setMapperClass(DataMapper.class);
        job.setMapOutputValueClass(CompositeValue.class);
        job.setMapOutputKeyClass(CompositeKey.class);

        //OUTPUT
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        //Secondary Sort
        job.setPartitionerClass(NaturalKeyPartitioner.class);
        job.setSortComparatorClass(FullKeyComparator.class);
        job.setGroupingComparatorClass(NaturalKeyComparator.class);

        //Reducer
        job.setReducerClass(VisitReducer.class);

        //Job start
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.epam.Main;
import com.epam.secondarySort.CompositeKey;
import com.epam.secondarySort.CompositeValue;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.hadoop.io.AvroSerialization;
import org.apache.avro.mapred.AvroKey;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

public class MapperReducerTest {
    MapDriver<AvroKey<GenericData.Record>, NullWritable, CompositeKey, CompositeValue> mapDriver;
    ReduceDriver<CompositeKey, CompositeValue, Text, IntWritable> reduceDriver;
    MapReduceDriver<AvroKey<GenericData.Record>, NullWritable, CompositeKey, CompositeValue, Text, IntWritable> mapReduceDriver;

    @Before
    public void setUp() {
        Main.DataMapper mapper = new Main.DataMapper();
        Main.VisitReducer reducer = new Main.VisitReducer();
        mapDriver = MapDriver.newMapDriver(mapper);
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
        mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);


        String[] strings = mapDriver.getConfiguration().getStrings("io.serializations");
        String[] newStrings = new String[strings.length + 1];
        System.arraycopy(strings, 0, newStrings, 0, strings.length);
        newStrings[newStrings.length - 1] = AvroSerialization.class.getName();

        //Now you have to configure AvroSerialization by specifying the key
        //writer Schema and the value writer schema.
        mapDriver.getConfiguration().setStrings("io.serializations", newStrings);
        mapDriver.getConfiguration().setStrings("avro.serialization.key.writer.schema", Main.SCHEMA.toString());
        mapDriver.getConfiguration().setStrings("avro.serialization.value.writer.schema", Main.SCHEMA.toString());
    }

    @Test
    public void testMapper() {
        AvroKey<GenericData.Record> key = new AvroKey<>(new GenericData.Record(Main.SCHEMA));
        key.datum().put("id", 546423L);
        key.datum().put("hotel_id", 224153L);
        key.datum().put("srch_ci", "2012-05-02");
        key.datum().put("srch_co", "2012-05-04");
        key.datum().put("srch_adults_cnt", 3);
        key.datum().put("channel", 2423);
        mapDriver.withInput(key, NullWritable.get());

        CompositeValue compositeValue = new CompositeValue();
        compositeValue.setChannel(2423);
        compositeValue.setLastVisited("2012-05-04");
        CompositeKey compositeKey = new CompositeKey();
        compositeKey.setHotel_id(224153L);
        compositeKey.setBooking_id(546423L);
        compositeKey.setSrch_ci("2012-05-02");

        mapDriver.withOutput(compositeKey, compositeValue);
        try {
            mapDriver.runTest();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testReducer() {
        CompositeKey compositeKey = new CompositeKey();
        compositeKey.setHotel_id(224153L);
        compositeKey.setBooking_id(546423L);
        compositeKey.setSrch_ci("2012-05-02");

        List<CompositeValue> values = new ArrayList<>();
        CompositeValue compositeValue = new CompositeValue();
        compositeValue.setChannel(2423);
        compositeValue.setLastVisited("2012-07-14");
        values.add(compositeValue);

        CompositeValue compositeValue1 = new CompositeValue();
        compositeValue1.setChannel(2123);
        compositeValue1.setLastVisited("2012-02-25");
        values.add(compositeValue1);

        CompositeValue compositeValue2 = new CompositeValue();
        compositeValue2.setChannel(1223);
        compositeValue2.setLastVisited("2012-03-15");
        values.add(compositeValue2);

        reduceDriver.withInput(compositeKey, values);
        reduceDriver.withOutput(new Text("224153"), new IntWritable(2423));

        try {
            reduceDriver.runTest();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
